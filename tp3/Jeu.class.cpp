#include <iostream>
#include "Jeu.class.h"
using namespace std;

/*=================================================
				INITIALISATION
=================================================*/


Jeu::Jeu(){
    Init();
	
}
Jeu::~Jeu(){}

void Jeu::Init(){
	bool choix;
	joueur = symboleJoueur1;

	cout << "0-MORPION | 1-PUISSANCE 4 : ";
	cin >> choix;
	if (choix) {
		grille4.AfficheGrille();
		JeuPuissance4();
	}
	else {
		grille.AfficheGrille();
		JeuMorpion();
	}

    
	
}

/*=================================================
					MORPION
=================================================*/
void Jeu::JeuMorpion()
{
	int x, y;
	while (!finDuJeu)
	{
		cout << "Joueur: " << joueur << endl;
		cout << "ligne: ";
		cin >> x;
		cout << "colonne: ";
		cin >> y;
		TesterValeurs(x, y);

		grille.AfficheGrille();

		if (grille.VictoireJoueur())
		{
			cout << joueur << " remporte la manche" << endl;
			finDuJeu = true;
		}
		else if (grille.MatchNul())
		{
			cout << "Match nul" << endl;
			finDuJeu = true;
		}
		else if (joueur == symboleJoueur1)
			joueur = symboleJoueur2;
		else
			joueur = symboleJoueur1;
	}
}
//TEST DE VALEURS LIGNE/COLONNE
void Jeu::TesterValeurs(int& x, int& y) {
	if (grille.DeposerJeton(x, y, joueur))
		grille.DeposerJeton(x, y, joueur);
	else
		while (true)
		{
			cout << "error" << endl;
			cout << "ligne: ";
			cin >> x;
			cout << "colonne: ";
			cin >> y;
			if (grille.DeposerJeton(x, y, joueur)) {
				grille.DeposerJeton(x, y, joueur);
				break;
			}

		}
}



/*=================================================
				PUISSANCE 4
=================================================*/

void Jeu::JeuPuissance4()
{
	int x, y;
	while (!finDuJeu)
	{
		cout << "Joueur: " << joueur << endl;
		cout << "ligne: ";
		cin >> x;
		cout << "colonne: ";
		cin >> y;
		TesterValeurs4(x, y);

		grille4.AfficheGrille();

		if (grille4.VictoireJoueur(joueur))
		{
			cout << joueur << " remporte la manche" << endl;
			finDuJeu = true;
		}
		else if (grille4.MatchNul())
		{
			cout << "Match nul" << endl;
			finDuJeu = true;
		}
		else if (joueur == symboleJoueur1)
			joueur = symboleJoueur2;
		else
			joueur = symboleJoueur1;
	}
}
//TEST DE VALEURS LIGNE/COLONNE
void Jeu::TesterValeurs4(int& x, int& y) {
	if (grille4.DeposerJeton(x, y, joueur))
		grille4.DeposerJeton(x, y, joueur);
	else
		while (true)
		{
			cout << "error" << endl;
			cout << "ligne: ";
			cin >> x;
			cout << "colonne: ";
			cin >> y;
			if (grille4.DeposerJeton(x, y, joueur)) {
				grille4.DeposerJeton(x, y, joueur);
				break;
			}

		}
}

