#pragma once

class GrilleMorpion{
public:
    GrilleMorpion();
    ~GrilleMorpion();
    
    bool CaseVide(const int&, const int&) const;
    bool DeposerJeton(const int&, const int&, const char&);
    
    bool LigneComplete() const;
    bool ColonneComplete() const;
    bool CrepeComplete() const;
    bool MatchNul() const;
    
    bool VictoireJoueur() const;
    
    void AfficheGrille() const;
    
private:
    char grille[3][3] ={
        {'*', '*', '*'},
        {'*', '*', '*'},
        {'*', '*', '*'}};
};
