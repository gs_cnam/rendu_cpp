#include <iostream>
#include "GrilleMorpion.class.h"

using namespace std;

GrilleMorpion::GrilleMorpion(){}
GrilleMorpion::~GrilleMorpion(){}
/*=================================================
				REMPLISSAGE GRILLE
=================================================*/
bool GrilleMorpion::CaseVide(const int& x, const int& y) const
{
	return grille[x][y] == '*';
}

bool GrilleMorpion::DeposerJeton(const int& x, const int& y, const char& symb)
{
	if (CaseVide(x, y) && (x > -1 && x < 4) && (y > -1 && y < 4)) {
		grille[x][y] = symb;
		return true;
	}
	return false;
}


/*=================================================
				TEST VICTOIRE / NULL
=================================================*/
//LIGNE
bool GrilleMorpion::LigneComplete() const{
    for(int ligne = 0; ligne < 3; ligne++){
        if(grille[ligne][0] == grille[ligne][1] &&
			grille[ligne][0] == grille[ligne][2] &&
			grille[ligne][0] != '*')
            return true;
    };
    return false;
}
//COLONNE
bool GrilleMorpion::ColonneComplete() const{
    for(int colonne = 0; colonne < 3; colonne++){
			if (grille[0][colonne] == grille[1][colonne] &&
				grille[0][colonne] == grille[2][colonne] &&
				grille[0][colonne] != '*')
            return true;
    };
    return false;
}
//DIAGONALE
bool GrilleMorpion::CrepeComplete() const{
    return ((grille[0][0] == grille[1][1] && grille[0][0] == grille[2][2]) ||
            (grille[0][2] == grille[1][1] && grille[0][2] == grille[2][0]) &&
			 grille[0][2] != '*');
}
//GRILLE PLEINE
bool GrilleMorpion::MatchNul() const{
    for(int ligne = 0; ligne < 3; ligne++){
        for(int colonne = 0; colonne < 3; colonne++){
            if(grille[ligne][colonne] == '*')
                return false;
        }
    }
    return true;
}

bool GrilleMorpion::VictoireJoueur() const{
	return(LigneComplete()    ||
			ColonneComplete() ||
			CrepeComplete());
}
/*=================================================
				AFFICHAGE
=================================================*/
void GrilleMorpion::AfficheGrille() const{
	cout << endl;
	cout << "  0 1 2" << endl;
    for(int ligne = 0; ligne < 3; ligne++){
		cout << ligne << " ";
        for(int colonne = 0; colonne < 3; colonne++){
            cout << grille[ligne][colonne] << " ";
        }
		cout << endl;
    }
}
