#include <iostream>
#include "Puissance4.h"

using namespace std;

Puissance4::Puissance4() {}
Puissance4::~Puissance4() {}


/*=================================================
				REMPLISSAGE GRILLE
=================================================*/
bool Puissance4::CaseVide(const int& x, const int& y) const
{
	return grille[x][y] == '*';
}

bool Puissance4::DeposerJeton(const int& x, const int& y, const char& symb)
{
	if (CaseVide(x, y) && (x > -1 && x < 5) && (y > -1 && y < 8)) {
		grille[x][y] = symb;
		return true;
	}
	return false;
}


/*=================================================
				TEST VICTOIRE / NULL
=================================================*/
//LIGNE
bool Puissance4::LigneComplete(const char& symb) const {
	
	for (int ligne = 0; ligne < 4; ligne++) {
		int nbJetons = 0;
		for (int colonne = 0; colonne < 7; colonne++) {
			if (nbJetons == 4)
				return true;
			else if (grille[ligne][colonne] == symb)
				nbJetons++;
			else
				nbJetons = 0;
		}
	};
	return false;
}
//COLONNE
bool Puissance4::ColonneComplete(const char& symb) const {
	for (int colonne = 0; colonne < 7; colonne++) {
		int nbJetons = 0;
		for (int ligne = 0; ligne < 4; ligne++) {
			if (grille[ligne][colonne] == symb)
				nbJetons++;
		}
		if (nbJetons == 4)
			return true;
	}
	return false;
}
//DIAGONALE
bool Puissance4::CrepeComplete() const {
	return (
		(grille[3][0] == grille[2][1] && grille[3][0] == grille[1][2] && grille[3][0] == grille[0][3]) && grille[3][0] != '*' ||
		(grille[3][1] == grille[2][2] && grille[3][1] == grille[1][3] && grille[3][1] == grille[0][4]) && grille[3][1] != '*' ||
		(grille[3][2] == grille[2][3] && grille[3][2] == grille[1][4] && grille[3][2] == grille[0][5]) && grille[3][2] != '*' ||
		(grille[3][3] == grille[2][4] && grille[3][3] == grille[1][5] && grille[3][3] == grille[0][6]) && grille[3][3] != '*' ||

		(grille[3][6] == grille[2][5] && grille[3][6] == grille[1][4] && grille[3][6] == grille[0][6]) && grille[3][6] != '*' ||
		(grille[3][5] == grille[2][4] && grille[3][5] == grille[1][3] && grille[3][5] == grille[0][5]) && grille[3][5] != '*' ||
		(grille[3][4] == grille[2][3] && grille[3][4] == grille[1][2] && grille[3][4] == grille[0][4]) && grille[3][4] != '*' ||
		(grille[3][3] == grille[2][2] && grille[3][3] == grille[1][1] && grille[3][3] == grille[0][3]) && grille[3][3] != '*'
	);
}
//GRILLE PLEINE
bool Puissance4::MatchNul() const {
	for (int ligne = 0; ligne < 4; ligne++) {
		for (int colonne = 0; colonne < 7; colonne++) {
			if (grille[ligne][colonne] == '*')
				return false;
		}
	}
	return true;
}

bool Puissance4::VictoireJoueur(const char& symb) const {
	return(LigneComplete(symb) ||
		ColonneComplete(symb) ||
		CrepeComplete());
}


/*=================================================
				AFFICHAGE
=================================================*/
void Puissance4::AfficheGrille() const {
	cout << endl;
	cout << "  0 1 2 3 4 5 6" << endl;
	for (int ligne = 0; ligne < 4; ligne++) {
		cout << ligne << " ";
		for (int colonne = 0; colonne < 7; colonne++) {
			cout << grille[ligne][colonne] << " ";
		}
		cout << endl;
	}
}
