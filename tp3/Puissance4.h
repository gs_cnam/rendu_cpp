#pragma once
class Puissance4
{
public:
	Puissance4();
	~Puissance4();

	bool CaseVide(const int&, const int&) const;
	bool DeposerJeton(const int&, const int&, const char&);

	bool LigneComplete(const char&) const;
	bool ColonneComplete(const char&) const;
	bool CrepeComplete() const;
	bool MatchNul() const;

	bool VictoireJoueur(const char&) const;

	void AfficheGrille() const;

private:
	char grille[4][7] = {
		{'*', '*', '*', '*', '*', '*', '*'},
		{'*', '*', '*', '*', '*', '*', '*'},
		{'*', '*', '*', '*', '*', '*', '*'},
		{'*', '*', '*', '*', '*', '*', '*'}};
};

