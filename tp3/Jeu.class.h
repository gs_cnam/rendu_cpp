#pragma once

#include "GrilleMorpion.class.h"
#include "Puissance4.h"
class Jeu{
public:
    Jeu();
    ~Jeu();
    void Init();
    void JeuMorpion();
	void JeuPuissance4();
    
	void TesterValeurs(int&, int&);
	void TesterValeurs4(int&, int&);
private:
	GrilleMorpion grille;
	Puissance4 grille4;
    const char symboleJoueur1 = 'X';
    const char symboleJoueur2 = 'O';
    bool finDuJeu = false;
	char joueur;

};