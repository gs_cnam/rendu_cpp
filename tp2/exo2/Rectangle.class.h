//
//  rectangle.hpp
#pragma once //On ne charge Rectangle q'une fois

#include <stdio.h>
#include "Point.struct.h"

class Rectangle {

public:
    Rectangle(int, int);
    ~Rectangle();

    void SetLongueur(int);
    void SetLargeur(int);
    void SetPoint(const Point&);
    
    int GetLongueur();
    int GetLargeur();
    Point GetPoint();
    
    int Perimetre() const;
    int Aire() const;
    bool TestPerimetreRectangle(const Rectangle&);
    bool TestSurfaceRectangle(const Rectangle&);
    
    void AfficheRectangle();
    
private:
    int longueur, largeur;
    Point pointHautGauche;
  
    
    
};
