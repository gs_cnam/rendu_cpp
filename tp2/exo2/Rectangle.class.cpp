#include <iostream>
#include "Rectangle.class.h"

using namespace std;

Rectangle::Rectangle(int lon, int lar){
    longueur = lon;
    largeur = lar;
    
}
Rectangle::~Rectangle(){}

//SETTERS
void Rectangle::SetLongueur(int x){
    longueur = x;
}
void Rectangle::SetLargeur(int x){
    largeur = x;
}
void Rectangle::SetPoint(const Point& p){
    pointHautGauche = p;
}

//GETTERS
int Rectangle::GetLongueur(){
    return longueur;
}
int Rectangle::GetLargeur(){
    return largeur;
}
Point Rectangle::GetPoint(){
    return pointHautGauche;
}


int Rectangle::Perimetre() const{
    return 2*(longueur+largeur);
}
int Rectangle::Aire() const{
    return longueur*largeur;
}

bool Rectangle::TestPerimetreRectangle(const Rectangle& rectangleATester){
    if(this->Perimetre() > rectangleATester.Perimetre())
        return true;
    else
        return false;
}
bool Rectangle::TestSurfaceRectangle(const Rectangle& rectangleATester){
    if(this->Aire() > rectangleATester.Aire())
        return true;
    else
        return false;
}

void Rectangle::AfficheRectangle(){
    cout << "Largeur du rectangle: " << largeur << endl;
    cout << "Longueur du rectangle: " << longueur << endl;
    cout << endl;
    cout << "Périmètre: " << this->Perimetre() << endl;
    cout << "Aire du rectangle: " << this->Aire() << endl;
    cout << endl;
}

