#include <iostream>
#include "Point.struct.h"
#include "Cercle.class.h"

int main(int argc, const char * argv[]) {
    Point centre = {0, 0};
    Cercle c1(centre, 6);
    
    c1.Afficher();
    
    return 0;
}
