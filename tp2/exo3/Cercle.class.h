#pragma once //Cercle ne sera chargé qu'une seule fois

#include <stdio.h>
#include "Point.struct.h"

class Cercle{
public:
    Cercle(const Point&, int);
    ~Cercle();
    
    Point GetCentre();
    int GetDiametre();
    
    void SetCentre(const Point&);
    void SetDiametre(int);
    
    float Perimetre() const;
    float Aire() const;
    
    bool PointSurLeCercle(const Point&);
    bool PointDansLeCercle(const Point&);
    
    void Afficher();
    
private:
    Point centre;
    int diametre;
};
