#include <iostream>
#include <math.h>
#include "Cercle.class.h"

using namespace std;

float DistanceEntre2Points(const Point& p1, const Point& p2);

Cercle::Cercle(const Point& p0, int d){
    diametre = d;
    centre = p0;
}

Cercle::~Cercle(){
    
}
//GETTERS
Point Cercle::GetCentre(){
    return centre;
}
int Cercle::GetDiametre(){
    return diametre;
}

//SETTERS
void Cercle::SetCentre(const Point& p){
    centre = p;
}
void Cercle::SetDiametre(int d){
    diametre = d;
}


float Cercle::Perimetre() const{
    return diametre * M_PI;
}

float Cercle::Aire() const{
    return diametre/2 * pow(M_PI, 2);
}

bool Cercle::PointSurLeCercle(const Point& p){
    if(DistanceEntre2Points(centre, p) == diametre/2)
        return true;
    else
        return false;
}

bool Cercle::PointDansLeCercle(const Point& p){
    if(DistanceEntre2Points(centre, p) < pow(diametre/2,2))
        return true;
    else
        return false;
}

void Cercle::Afficher(){
    cout << "Diamètre du cercle : " << diametre << endl;
    cout << "Périmètre du cercle : " << this->Perimetre() << endl;
    cout << "Aire du cercle : " << this->Aire() << endl;
    
}

float DistanceEntre2Points(const Point& p1, const Point& p2){
    return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}
