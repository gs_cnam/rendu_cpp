#include <math.h>
#include "Triangle.class.h"
#include <iostream>

float DistanceEntre2Points(const Point& p1, const Point& p2);
bool PlusGrandeDistance(float x, float y);

Triangle::Triangle(const Point& x, const Point& y, const Point& z){
    a = x;
    b = y;
    c = z;
    
    cote1 = DistanceEntre2Points(a, b);
    cote2 = DistanceEntre2Points(a, c);
    cote3 = DistanceEntre2Points(b, c);
}
Triangle::~Triangle(){}

Point Triangle::GetPointA(){
    return a;
}
Point Triangle::GetPointB(){
    return b;
}
Point Triangle::GetPointC(){
    return c;
}

void Triangle::SetPointA(const Point& x){
    a = x;
}
void Triangle::SetPointB(const Point& y){
    b = y;
}
void Triangle::SetPointC(const Point& z){
    c = z;
}

//Le côté le plus grand sera choisi comme base.
float Triangle::Base() const{
    if(PlusGrandeDistance(cote1, cote2) && PlusGrandeDistance(cote1, cote3))
        return cote1;
    else if(PlusGrandeDistance(cote2, cote1) && PlusGrandeDistance(cote2, cote3))
        return cote2;
    else
        return cote3;
}

float Triangle::Perimetre() const{
    return cote1 + cote2 + cote3;
}

//Calcul de l'aire à partir de la formule de Héron
float Triangle::Aire() const{
    float p = this->Perimetre()/2;
    return sqrtf(p * (p - cote1) * (p - cote2) * (p - cote3));
}

//Calcul de l'aire à partir de la formule aire = base * hauteur / 2
float Triangle::Hauteur() const{
    return 2 * this->Aire() / this->Base();
}

bool Triangle::EstIsocele(){
    if(cote1 == cote2 ||
       cote1 == cote3 ||
       cote2 == cote3)
        return true;
    else
        return false;
}
bool Triangle::EstEquilateral(){
    if(cote1 == cote2 &&
       cote1 == cote3 &&
       cote2 == cote3)
        return true;
    else
        return false;
}

//On test la forlule de Pythagore sur tous les côtés
bool Triangle::EstRectangle(){
    if(pow(cote1,2) == pow(cote2,2) +  pow(cote3,2) ||
       pow(cote2,2) == pow(cote1,2) +  pow(cote3,2) ||
       pow(cote3,2) == pow(cote1,2) +  pow(cote2,2))
        return true;
    else
        return false;
}

void Triangle::AfficherParametres(){
    cout << "côté 1 = " << cote1 << endl;
    cout << "côté 2 = " << cote2 << endl;
    cout << "côté 3 = " << cote3 << endl;
    cout << endl;
    cout << "Base du triangle = " << this->Base() << endl;
    cout << "Hauteur du triangle = " << this->Hauteur() << endl;
    cout << "Périmètre du triangle = " << this->Perimetre() << endl;
    cout << "Aire du triangle = " << this->Aire() << endl;
    cout << endl;
    
}

float DistanceEntre2Points(const Point& p1, const Point& p2){
    return sqrtf(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

bool PlusGrandeDistance(float x, float y){
    if(x > y)
        return true;
    else
        return false;
}
