#include <iostream>
#include "Triangle.class.h"
#include "Point.struct.h"

int main(int argc, const char * argv[]) {
    Point a = {5, 0};
    Point b = {7, 3};
    Point c = {2, 9};
    
    Triangle t1(a, b, c);
    t1.AfficherParametres();
    
    return 0;
}
