#pragma once //car Triangle peut être utilisé à plusieurs endroits

#include <iostream>
#include "Point.struct.h"

using namespace std;

class Triangle{
public:
    Triangle(const Point&, const Point&, const Point&);
    ~Triangle();
    
    Point GetPointA();
    Point GetPointB();
    Point GetPointC();
    
    void SetPointA(const Point&);
    void SetPointB(const Point&);
    void SetPointC(const Point&);
    
    float Base() const;
    float Perimetre() const;
    float Aire() const;
    float Hauteur() const;
    
    bool EstIsocele();
    bool EstRectangle();
    bool EstEquilateral();
    
    void AfficherParametres();
    
private:
    Point a, b, c;
    float cote1, cote2, cote3;
};
