#include <iostream>
#include "Point.struct.cpp"

using namespace std;

void Afficher(Point p);

int main(){
    Point test = {20, 6};
    Afficher(test);
}

void Afficher(Point p){
    cout << "x : " << p.x << endl;
    cout << "y : " << p.y << endl;
}
