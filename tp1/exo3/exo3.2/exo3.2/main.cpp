//
//  main.cpp
//  exo3.2
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

void UserVsAI();
void AIVsUser();

int main(int argc, const char * argv[]) {
    
    int choice = 0;
    
    cout << "0-L'IA devine un nombre" << endl;
    cout << "1-Le joueur devine un nombre" << endl;
    cin >> choice;
    while(!cin || (choice != 0 && choice != 1))
    {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Saisie incorrecte" << endl;
        cout << "Votre choix : ";
        cin >> choice;
    }
    
    switch (choice) {
        case 0:
            AIVsUser();
            break;
        case 1:
            UserVsAI();
            break;
    }
    return 0;
}

void UserVsAI(){
    int randomNumber = rand()%1000;
    int enteredNumber = 0;
    while (true) {
        cout << "Entrez votre nombre : ";
        cin >> enteredNumber;
        while(!cin)
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "Saisie incorrecte" << endl;
            cout << "Entrez votre nombre : ";
            cin >> enteredNumber;
        }
        
        if(enteredNumber < randomNumber)
            cout << "C'est plus !" << endl;
        else if(enteredNumber > randomNumber)
            cout << "C'est moins !" << endl;
        else if(enteredNumber == randomNumber){
            cout << "Bien joué ! Le nombre à deviner était bien le : " << randomNumber << endl;
            break;
        }
    }
}

void AIVsUser(){
    int numberToGuess = 2000;
    int numberGuessed = 500, lowBound = 0, highBound = 1000;
    int userInput = 0;
    int numberOfRound = 0;
    
    cout << "Entrez votre nombre (entre O et 1000) : ";
    cin >> numberToGuess;
    while(!cin || numberToGuess < 0 || numberToGuess > 1000)
    {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Saisie incorrecte" << endl;
        cout << "Entrez votre nombre (entre O et 1000) : ";
        cin >> numberToGuess;
    }
    
    while (true) {
        numberOfRound++;
        
        cout << "L'IA devine : " << numberGuessed << endl;
        cout << "1- l'ordinateur à deviné" << endl
        << "2- Le nombre deviné est trop grand" << endl
        << "3- Le nombre deviné est trop petit" << endl;
        cin >> userInput;
        while(!cin)
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "Saisie incorrecte" << endl;
            cout << "Entrez votre choix : ";
            cin >> userInput;
        }
        
        switch (userInput) {
            case 1:
                cout << "L'IA a trouvé en " << numberOfRound << " essai(s)" << endl;
                return;
            case 2:
                highBound = numberGuessed;
                numberGuessed = numberGuessed - (numberGuessed - lowBound)/2;
                break;
            case 3:
                lowBound = numberGuessed;
                numberGuessed = numberGuessed + (highBound - numberGuessed)/2;
                break;
            default:
                break;
        }
    }
}
