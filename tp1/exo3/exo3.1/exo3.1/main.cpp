//
//  main.cpp
//  exo3.1
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;


int main(int argc, const char * argv[]) {
    
    string name = "";

    cout << "Entrez votre prénom et votre nom : ";
    getline(cin, name);

    cout << name << endl;
    for(int i = 0; i < name.length(); i++){
        if(i == 0){name[i] = toupper(name[i]);}
        else if(name[i] == ' '){
            for(int j = i +1; j < name.length(); j++){
                name[j] = toupper(name[j]);
            }
        }
    }
    cout << "Salut, " << name << endl;
    
    return 0;
}
