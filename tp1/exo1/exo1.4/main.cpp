//
//  main.cpp
//  exo1.4
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

void CreateTabRandomInt(int *tab, int length);
void DisplayTab(int *tab, int length);
void Reverse2(int &a, int &b);
void SortTab(int *tab, int length, bool ascend = true);

int main(int argc, const char * argv[]) {
    
    int tabLength = 0;
    bool ascendSort = true;
    
    cout << "Taille du tableau : ";
    cin >> tabLength;
    int tab[tabLength];
    
    cout << "1-tri croissant" << endl;
    cout << "0-tri décroissant" << endl;
    cout << "choix : ";
    cin >> ascendSort;
    
    CreateTabRandomInt(tab, tabLength);
    cout << "Tableau généré : " << endl;
    DisplayTab(tab, tabLength);
    SortTab(tab, tabLength, ascendSort);
    cout << "Après le tri choisi : " << endl;
    DisplayTab(tab, tabLength);
    SortTab(tab, tabLength, !ascendSort);
    cout << "L'autre tri disponible : " << endl;
    DisplayTab(tab, tabLength);
    return 0;
}

void CreateTabRandomInt(int *tab, int length){
    for(int i = 0; i < length; i++){
        tab[i] = rand()%length;
    }
}

void DisplayTab(int *tab, int length){
    for(int i = 0; i < length; i++){
        cout << tab[i] << " ";
    }
    cout << endl;
}

void Reverse2(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

void SortTab(int *tab, int length, bool ascend ){
    
    for(int i = 0; i < length; i++){
        for(int j = i + 1; j < length; j++){
            if(ascend && tab[i] > tab[j])
                Reverse2(tab[i], tab[j]);
            else if(!ascend && tab[i] < tab[j])
                Reverse2(tab[i], tab[j]);
        }
    }
    cout << endl;
    
}
