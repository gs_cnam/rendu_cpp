//
//  main.cpp
//  exo1.1
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

int AddInt2(int a, int b);

int main(int argc, const char * argv[]) {
    
    cout << "Somme de 1+2 = " << AddInt2(1, 2) << endl;
    
    return 0;
}

int AddInt2(int a, int b){
    return a + b;
}
