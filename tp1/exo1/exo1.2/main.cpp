//
//  main.cpp
//  exo1.2
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

void Reverse2(int &a, int &b);

int main(int argc, const char * argv[]) {
    int a = 1, b = 2;
    
    cout << "Valeur initial de a : " << a << endl;
    cout << "Valeur initial de b : " << b << endl;
    
    Reverse2(a, b);
    cout << "Après inversion : " << endl;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    
    return 0;
}

void Reverse2(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}
