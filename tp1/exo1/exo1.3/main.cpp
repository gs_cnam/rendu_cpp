//
//  main.cpp
//  exo1.3
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

void AddPtrInt2(int *a, int *b, int *c);
void AddRefInt2(int &a, int &b, int &c);

int main(int argc, const char * argv[]) {
    
    int a, b, c;
    a = b = c = 3;
    
    cout << "Valeur initial : " << endl;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    AddPtrInt2(&a, &b, &c);
    cout << "Après l'appel de la fonctiob pointeur : " << endl;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    AddRefInt2(a, b, c);
    cout << "Après l'appel de la fonctiob référence : " << endl;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    
    return 0;
}

void AddPtrInt2(int *a, int *b, int *c){
    *c = *a + *b;
}

void AddRefInt2(int &a, int &b, int &c){
    c = a + b;
}
