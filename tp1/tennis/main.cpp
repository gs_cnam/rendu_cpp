//
//  main.cpp
//  tennis
//
//  Created by Guillaume Schmidt on 28/09/2021.
//

#include <iostream>
using namespace std;

void DisplayScore (int p1, int p2);

int main(int argc, const char * argv[]) {
    
    int scoreP1 = 0, scoreP2 = 0;
    cout << "Nombre de manches gagné par le joueur 1 : ";
    cin >> scoreP1;
    cout << "Nombre de manches gagné par le joueur 2 : ";
    cin >> scoreP2;
    
    
    DisplayScore(scoreP1,scoreP2);
    return 0;
}

void DisplayScore (int p1, int p2){
    
    int tab[2][5] = {{0, 1, 2, 3, 4},{0, 15, 30, 40}};
    
    if(p1 >= 3 && p2 >= 3){
        if(p1 > p2){
            if(p1-p2 ==2){cout << "Victoire du joueur 1" << endl;}
            else{cout << "Aventage joueur 1" << endl;}
        }else if(p2 > p1){
            if(p1-p2 ==2){cout << "Victoire du joueur 2" << endl;}
            else{cout << "Aventage joueur 2" << endl;}
        }
        else{
            cout << "Score du joueur 1 : 40" << endl;
            cout << "Score du joueur 2 : 40" << endl;
        }
    }
    else{
        for(int i = 0; i < 1; i++){
            for(int j = 0; j < 6; j++){
                if(p1 == tab[0][j]){cout << "Score du joueur 1 : " << tab[1][j] << endl;}
                if(p2 == tab[0][j]){cout << "Score du joueur 2 : " << tab[1][j] << endl;}
            }
        }
    }
    
}
