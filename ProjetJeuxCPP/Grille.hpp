
#ifndef Grille_hpp
#define Grille_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Player.hpp"

using namespace std;

class Grille
{
protected:
    int width, height;
    vector<vector<char>> grille;
public:
    Grille(int inputWidth, int inputHeight){};
    ~Grille(){};
    virtual void InitBoard() = 0;
    virtual bool MatchNul() const = 0;
    virtual bool VictoireJoueur(const char& symb) const = 0;
    virtual void AfficheGrille() const = 0;
    
	virtual bool DeposerJeton(const int&, const int&, const char&) { return false; };
	virtual bool DeposerJeton(const int&, const char&) { return false; };
	virtual bool DeposerJeton(const int& x, const int& y, const char& player, const char& ennemi) { return false; };
};

#endif /* Grille_hpp */
