#include <iostream>
#include "GrilleMorpion.hpp"

using namespace std;

GrilleMorpion::GrilleMorpion(int inputWidth, int inputHeight): Grille(inputWidth, inputHeight)
{
    width = inputWidth;
    height = inputHeight;
}

void GrilleMorpion::InitBoard(){
    vector<char> v;
    for (int i = 0; i < width; i++)
        v.push_back(' ');

    for (int i = 0; i < height; i++)
        grille.push_back(v);
}

bool GrilleMorpion::CaseVide(const int& x, const int& y) const
{
	return grille[x][y] == ' ';
}

bool GrilleMorpion::DeposerJeton(const int& x, const int& y, const char& symb)
{
	cout << "test";
	if (CaseVide(x, y) && (x >= 0 && x <= width) && (y >= 0 && y <=  height)) {
		grille[x][y] = symb;
		return true;
    }
    return false;
}


//LIGNE
bool GrilleMorpion::LigneComplete() const{
    for(int ligne = 0; ligne < 3; ligne++){
        if(grille[ligne][0] == grille[ligne][1] &&
			grille[ligne][0] == grille[ligne][2] &&
			grille[ligne][0] != ' ')
            return true;
    };
    return false;
}
//COLONNE
bool GrilleMorpion::ColonneComplete() const{
    for(int colonne = 0; colonne < 3; colonne++){
			if (grille[0][colonne] == grille[1][colonne] &&
				grille[0][colonne] == grille[2][colonne] &&
				grille[0][colonne] != ' ')
            return true;
    };
    return false;
}
//DIAGONALE
bool GrilleMorpion::DiagonaleComplete() const{
    return (
            ((grille[0][0] == grille[1][1] && grille[0][0] == grille[2][2]) ||
            (grille[0][2] == grille[1][1] && grille[0][2] == grille[2][0]))
            && (grille[0][2] != ' ')
            );
}
//GRILLE PLEINE
bool GrilleMorpion::MatchNul() const{
    for(int ligne = 0; ligne < width; ligne++){
        for(int colonne = 0; colonne < height; colonne++){
            if(grille[ligne][colonne] == ' ')
                return false;
        }
    }
    return true;
}

bool GrilleMorpion::VictoireJoueur(const char& symb) const{
	return(LigneComplete()    ||
			ColonneComplete() ||
           DiagonaleComplete());
}


void GrilleMorpion::AfficheGrille() const{
    cout << "   0     1     2  " << endl;
    cout << "      |     |     " << endl;
    cout << "0  " << grille[0][0] << "  |  " << grille[0][1] << "  |  " << grille[0][2] << endl;

    cout << " _____|_____|_____" << endl;
    cout << "      |     |     " << endl;

    cout << "1  " << grille[1][0] << "  |  " << grille[1][1] << "  |  " << grille[1][2] << endl;

    cout << " _____|_____|_____" << endl;
    cout << "      |     |     " << endl;

    cout << "2  " << grille[2][0] << "  |  " << grille[2][1] << "  |  " << grille[2][2] << endl;

    cout << "      |     |     " << endl << endl;
}
