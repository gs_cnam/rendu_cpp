#ifndef GrilleOthello_hpp
#define GrilleOthello_hpp

#include <stdio.h>

#include "Grille.hpp"
#include "Player.hpp"

class GrilleOthello: public Grille{
public:
    GrilleOthello(int inputWidth, int inputHeight);
    ~GrilleOthello(){};
    
    void InitBoard() override;
    
    bool CaseVide(const int&, const int&) const {return false;};
    bool DeposerJeton(const int& x, const int& y, const char& player, const char& ennemi) override;
 
    bool MatchNul() const override {return false;};
    bool VictoireJoueur(const char& symb) const override {return false;};
    void AfficheGrille() const override;
    
private:
	bool CanPlaceToken(const char& player, const char& ennemi, int x, int y);

    bool CheckLeft(const char& player, const char& ennemi, int x, int y);
    bool CheckRight(const char& player, const char& ennemi, int x, int y);
    bool CheckTop(const char& player, const char& ennemi, int x, int y);
    bool CheckBottom(const char& player, const char& ennemi, int x, int y);
	bool CheckDLeft(const char& player, const char& ennemi, int x, int y);
	bool CheckDRight(const char& player, const char& ennemi, int x, int y);
	bool CheckDTop(const char& player, const char& ennemi, int x, int y);
	bool CheckDBottom(const char& player, const char& ennemi, int x, int y);

    void SwitchLeft(const char& player, const char& ennemi, int x, int y);
    void SwitchRight(const char& player, const char& ennemi, int x, int y);
    void SwitchTop(const char& player, const char& ennemi, int x, int y);
    void SwitchBottom(const char& player, const char& ennemi, int x, int y);
	void SwitchDLeft(const char& player, const char& ennemi, int x, int y);
	void SwitchDRight(const char& player, const char& ennemi, int x, int y);
	void SwitchDTop(const char& player, const char& ennemi, int x, int y);
	void SwitchDBottom(const char& player, const char& ennemi, int x, int y);
};



#endif /* GrilleOthello_hpp */
