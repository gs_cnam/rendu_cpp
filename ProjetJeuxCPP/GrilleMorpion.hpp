#pragma once

#include "Grille.hpp"

class GrilleMorpion: public Grille{
public:
    GrilleMorpion(int inputWidth, int inputHeight);
    ~GrilleMorpion(){};
    
    void InitBoard() override;
    
    bool CaseVide(const int&, const int&) const;
    bool DeposerJeton(const int&, const int&, const char&) override;
    
    bool LigneComplete() const;
    bool ColonneComplete() const;
    bool DiagonaleComplete() const;
    
    bool MatchNul() const override;
    bool VictoireJoueur(const char& symb) const override;
    void AfficheGrille() const override;
    
};
