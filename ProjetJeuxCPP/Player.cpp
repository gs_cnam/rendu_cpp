#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Player.hpp"


Player::Player(std::string inputName, char inputSymbol) {
	name = inputName;
	symbol = inputSymbol;

}

char Player::GetSymbol() { return symbol; }

std::string Player::GetNom() { return name; }

std::string Player::GetPlayerData() {
	return "nom: " + name + " symbole: " + symbol;
}