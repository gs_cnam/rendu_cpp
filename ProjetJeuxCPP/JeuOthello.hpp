#pragma once
#include <iostream>
#include "Grille.hpp"
class JeuOthello
{
public:
	JeuOthello(std::shared_ptr<Grille>& inputGrille, shared_ptr<iPlayer>& inputP1, shared_ptr<iPlayer>& inputP2);
	void InitGame();
	void JeuEnCours();
	void TesterValeurs();
	bool finDuJeu = false;

	int ligne, colonne;
	shared_ptr<Grille> grille;
	shared_ptr<iPlayer> p1, p2, joueur, ennemi;

};

