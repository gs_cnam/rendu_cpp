#pragma once
#include "Grille.hpp"

class GrillePuissance4: public Grille
{
public:
    GrillePuissance4(int inputWidth, int inputHeight);
    ~GrillePuissance4(){};

    void InitBoard() override;
    
    bool CaseVide(const int&, const int&) const;
    bool DeposerJeton(const int&, const char&) override;
    
    bool LigneComplete(const char& symb) const;
    bool ColonneComplete(const char& symb) const;
    bool DiagonaleComplete(const char& symb) const;
    
    bool MatchNul() const override;
    bool VictoireJoueur(const char& symb) const override;
    void AfficheGrille() const override;
};

