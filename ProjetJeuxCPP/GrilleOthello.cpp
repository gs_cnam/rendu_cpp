#include "GrilleOthello.hpp"

GrilleOthello::GrilleOthello(int inputWidth, int inputHeight): Grille(inputWidth, inputHeight)
{
    width = inputWidth;
    height = inputHeight;
}

void GrilleOthello::InitBoard(){
    vector<char> v;
    for (int i = 0; i < width; i++)
        v.push_back(' ');

    for (int i = 0; i < height; i++)
        grille.push_back(v);

	grille[3][3] = 'X';
	grille[3][4] = 'O';
	grille[4][3] = 'O';
	grille[4][4] = 'X';
}

void GrilleOthello::AfficheGrille() const{
    cout << "    0   1   2   3   4   5   6   7" << endl;
    int row  = 0;          /* Row index      */
    int col = 0;           /* Column index   */
    char col_label = 'a';  /* Column label   */

    /* Display the intermediate rows */
    for(row = 0; row < height; row++)
    {
      cout << "  +";
      for(col = 0; col<height; col++)
        printf("---+");
	  cout << endl << row << " |";

      for(col = 0; col<height; col++)
        printf(" %c |", grille[row][col]);  /* Display counters in row */
	  cout << endl;
    }

    printf("  +");                  /* Start the bottom line   */
    for(col = 0 ; col<height ;col++)
      printf("---+");               /* Display the bottom line */
	cout << endl;                   /* End the bottom  line    */
}



//to the left
bool GrilleOthello::CheckLeft(const char& player, const char& ennemi, int x, int y) {
    for (int i = x; i >= 0; i--) {
        if (grille[i][y] == ennemi && grille[i - 1][y] == player)
            return true;
    }
    return false;
}
//to the right
bool GrilleOthello::CheckRight(const char& player, const char& ennemi, int x, int y)  {
    for (int i = x; i < width; i++) {
        if (grille[i][y] == ennemi && grille[i + 1][y] == player)
            return true;
    }
    return false;
}
//to the top
bool GrilleOthello::CheckTop(const char& player, const char& ennemi, int x, int y)  {
    for (int i = x; i >= 0; i--) {
        if (grille[x][i] == ennemi && grille[x][i - 1] == player)
            return true;
    }
    return false;
}
//to the bottom
bool GrilleOthello::CheckBottom(const char& player, const char& ennemi, int x, int y)  {
    for (int i = x; i < width; i++) {
        if (grille[x][i] == ennemi && grille[x][i + 1] == player)
            return true;
    }
    return false;
}


//to the top left
bool GrilleOthello::CheckDLeft(const char& player, const char& ennemi, int x, int y) {
	for (int i = x; i >= 0; i--) {
		for (int j = y; j >= 0; j++) {
			if (i == 0 || j == 0 || grille[i][j] == ' ')
				break;
			if (grille[i][j] == ennemi && grille[i - 1][j - 1] == player)
				return true;
		}
	}
	return false;
}
//to the top right
bool GrilleOthello::CheckDRight(const char& player, const char& ennemi, int x, int y) {
	for (int i = x; i <= width; i++) {
		for (int j = y; j >= 0; j--) {
			if (i == width || j == 0 || grille[i][j] == ' ')
				break;
			if (grille[i][j] == ennemi && grille[i - 1][j + 1] == player)
				return true;
		}
	}
	return false;
}
//to the bottom left
bool GrilleOthello::CheckDTop(const char& player, const char& ennemi, int x, int y) {
	for (int i = x; i >= 0; i--) {
		for (int j = y; j <= width; j++) {
			if (i == 0 || j == width || grille[i][j] == ' ')
				break;
			if (grille[i][j] == ennemi && grille[i + 1][j - 1] == player)
				return true;
		}
	}
	return false;
}
//to the bottom right
bool GrilleOthello::CheckDBottom(const char& player, const char& ennemi, int x, int y) {
	for (int i = x; i <= width; i++) {
		for (int j = y; j <= width; j++) {
			if (i == width || j == width || grille[i][j] == ' ')
				break;
			if (grille[i][j] == ennemi && grille[i + 1][j + 1] == player)
				return true;
		}
	}
	return false;
}





//to the left
void GrilleOthello::SwitchLeft(const char& player, const char& ennemi, int x, int y) {
    
    for (int i = x; i >= 0; i--) {
        grille[i][y] = player;
        if (grille[i - 1][y] == player)
            break;
    }

}

//to the right
void GrilleOthello::SwitchRight(const char& player, const char& ennemi, int x, int y) {
    
    for (int i = x; i < width; i++) {
        grille[i][y] = player;
        if (grille[i + 1][y] == player)
            break;
    }

}
//to the top
void GrilleOthello::SwitchTop(const char& player, const char& ennemi, int x, int y) {
    
    for (int i = x; i >= 0; i--) {
        grille[x][i] = player;
        if (grille[x][i - 1] == player)
            break;
    }

}
//to the bottom
void GrilleOthello::SwitchBottom(const char& player, const char& ennemi, int x, int y) {
    
    for (int i = x; i < width; i++) {
        grille[x][i] = player;
        if (grille[x][i + 1] == player)
            break;
    }
}


//to the top left
void GrilleOthello::SwitchDLeft(const char& player, const char& ennemi, int x, int y) {

	for (int i = x; i >= 0; i--) {
		for (int j = y; j >= 0; j++) {
			grille[i][j] = player;
			if (grille[i - 1][j - 1] == player)
				break;
		}
	}

}

//to the top right
void GrilleOthello::SwitchDRight(const char& player, const char& ennemi, int x, int y) {

	for (int i = x; i <= width; i++) {
		for (int j = y; j >= 0; j--) {
			grille[i][j] = player;
			if (grille[i - 1][j + 1] == player)
				break;
		}
	}

}
//to the bottom left
void GrilleOthello::SwitchDTop(const char& player, const char& ennemi, int x, int y) {

	for (int i = x; i >= 0; i--) {
		for (int j = y; j <= width; j++) {
			grille[i][j] = player;
			if (grille[i + 1][j - 1] == player)
				break;
		}
	}
}
//to the bottom right
void GrilleOthello::SwitchDBottom(const char& player, const char& ennemi, int x, int y) {

	for (int i = x; i <= width; i++) {
		for (int j = y; j <= width; j++) {
			grille[i][j] = player;
			if (grille[i][j] == ennemi && grille[i + 1][j + 1] == player)
				break;
		}
	}
}




bool GrilleOthello::CanPlaceToken(const char& player, const char& ennemi, int x, int y) {
    bool ennemiSpoted = false;

    //check if empty slot
    if (grille[x][y] != ' ')
        return false;

    //check if piece is around
    //
    //      -1 -1  -1 0  -1 1
    //       0 -1    X    1 0
    //      -1  1   1 0   1 1
    //
    if (grille[x - 1][y - 1] == ' ' && grille[x - 1][y] == ' ' && grille[x - 1][y + 1] == ' '
        && grille[x][y - 1] == ' ' && grille[x + 1][y] == ' '
        && grille[x - 1][y + 1] == ' ' && grille[x + 1][y] == ' ' && grille[x + 1][y + 1] == ' ')
        return false;

    //Check if the play is valid
    if (!CheckLeft(player, ennemi, x, y) && !CheckRight(player, ennemi, x, y) && !CheckTop(player, ennemi, x, y) && !CheckBottom( player, ennemi, x, y))
        return false;
    return true;
}

bool GrilleOthello::DeposerJeton(const int& x, const int& y, const char& player, const char& ennemi)
{
    if (CanPlaceToken(player, ennemi, x, y)) {
        grille[x][y] = player;
        if (CheckLeft( player, ennemi, x, y)) {
            SwitchLeft(player, ennemi, x, y);
        }
        else if(CheckRight(player, ennemi, x, y)) {
            SwitchRight(player, ennemi, x, y);
        }
        else if (CheckTop(player, ennemi, x, y)) {
            SwitchTop(player, ennemi, x, y);
        }
        else if (CheckBottom(player, ennemi, x, y)) {
            SwitchBottom(player, ennemi, x, y);
        }
		else if (CheckDLeft(player, ennemi, x, y)) {
			SwitchDLeft(player, ennemi, x, y);
		}
		else if (CheckDRight(player, ennemi, x, y)) {
			SwitchDRight(player, ennemi, x, y);
		}
		else if (CheckDTop(player, ennemi, x, y)) {
			SwitchDTop(player, ennemi, x, y);
		}
		else if (CheckDBottom(player, ennemi, x, y)) {
			SwitchDBottom(player, ennemi, x, y);
		}

		return true;
    }
	else 
	{
		//cout << "Case non jouable" << endl;
		return false;
	}
}
