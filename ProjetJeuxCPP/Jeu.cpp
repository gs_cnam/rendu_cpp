#include <iostream>
#include "Jeu.hpp"
using namespace std;


Jeu::Jeu(){
	Init();
}

void Jeu::Init(){
	int choix;
	p1 = make_shared<Player>("Player X", 'X');
	p2 = make_shared<Player>("Player O", 'O');

	cout << "0-MORPION" << endl;
    cout << "1-PUISSANCE 4" << endl;
    cout << "2-OTHELLO" << endl;
    cout << "Votre choix:";
	cin >> choix;
    
	if (choix == 0) {
		grille = make_shared<GrilleMorpion>(3, 3);
		JeuMorpion jeu(grille, p1, p2);
	}
        
	else if (choix == 1) {
		grille = make_shared<GrillePuissance4>(7, 4);
		JeuPuissance4 jeu(grille, p1, p2);
	}
        
	else if (choix == 2) {
		grille = make_shared<GrilleOthello>(8, 8);
		JeuOthello jeu(grille, p1, p2);
	}
        
}





