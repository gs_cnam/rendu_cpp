#include "JeuMorpion.hpp"
JeuMorpion::JeuMorpion(shared_ptr<Grille>& inputGrille, shared_ptr<iPlayer>& inputP1, shared_ptr<iPlayer>& inputP2)
{
	grille = inputGrille;
	p1 = inputP1;
	p2 = inputP2;
	InitGame();
}

void JeuMorpion::InitGame()
{
	cout << "Puissance 4:" << endl << endl;
	grille->InitBoard();
	grille->AfficheGrille();
	joueur = p1;
	JeuEnCours();
}


void JeuMorpion::JeuEnCours()
{
	while (!finDuJeu)
	{
		cout << "Tour de " << joueur->GetNom() << endl;
		cout << "ligne: ";
		cin >> ligne;
		cout << "colonne: ";
		cin >> colonne;
		grille->DeposerJeton(ligne, colonne, joueur->GetSymbol());

		grille->AfficheGrille();

		if (grille->VictoireJoueur(joueur->GetSymbol()))
		{
			cout << joueur->GetNom() << " remporte la manche" << endl;
			finDuJeu = true;
		}
		else if (grille->MatchNul())
		{
			cout << "Match nul" << endl;
			finDuJeu = true;
		}
		else if (joueur->GetSymbol() == p1->GetSymbol())
			joueur = p2;
		else
			joueur = p1;
	}
}
//TEST DE VALEURS LIGNE/COLONNE
void JeuMorpion::TesterValeurs() {
	if (grille->DeposerJeton(ligne, colonne, joueur->GetSymbol()))
		grille->DeposerJeton(ligne, colonne, joueur->GetSymbol());
	else
		while (true)
		{
			cout << "error" << endl;
			cout << "ligne: ";
			cin >> ligne;
			cout << "colonne: ";
			cin >> colonne;
			if (grille->DeposerJeton(ligne, colonne, joueur->GetSymbol())) {
				grille->DeposerJeton(ligne, colonne, joueur->GetSymbol());
				break;
			}

		}
}