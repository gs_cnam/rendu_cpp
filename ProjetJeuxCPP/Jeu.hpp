#pragma once

#include "GrilleMorpion.hpp"
#include "GrillePuissance4.hpp"
#include "GrilleOthello.hpp"

#include "JeuPuissance4.hpp"
#include "JeuMorpion.hpp"
#include "JeuOthello.hpp"

class Jeu{
public:
    Jeu();
    ~Jeu(){};
    
	

protected:
	void Init();

    

	shared_ptr<Grille> grille;
	shared_ptr<iPlayer> p1, p2, joueur;
};
