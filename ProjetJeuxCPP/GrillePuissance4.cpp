#include <iostream>
#include "GrillePuissance4.hpp"

using namespace std;

GrillePuissance4::GrillePuissance4(int inputWidth, int inputHeight): Grille(inputWidth, inputHeight)
{
    width = inputWidth;
    height = inputHeight;
}


void GrillePuissance4::InitBoard(){
    vector<char> v;
    for (int i = 0; i < width; i++)
        v.push_back(' ');

    for (int i = 0; i < height; i++)
        grille.push_back(v);
}

bool GrillePuissance4::CaseVide(const int& x, const int& y) const
{
	return grille[x][y] == ' ';
}

bool GrillePuissance4::DeposerJeton(const int& x, const char& symb)
{
	for (int i = 0; i < height-1; i++) {
		cout << i << " ";
		if (grille[i + 1][x] != ' ') {
			grille[i][x] = symb;
			break;
		}

		else if (i == height - 2)
			grille[height - 1][x] = symb;
	}
	return true;
}


//LIGNE
bool GrillePuissance4::LigneComplete(const char& symb) const {
	
	for (int ligne = 0; ligne < height; ligne++) {
		int nbJetons = 0;
		for (int colonne = 0; colonne < width; colonne++) {
			if (nbJetons == 4)
				return true;
			else if (grille[ligne][colonne] == symb)
				nbJetons++;
			else
				nbJetons = 0;
		}
	};
	return false;
}
//COLONNE
bool GrillePuissance4::ColonneComplete(const char& symb) const {
	for (int colonne = 0; colonne < width; colonne++) {
		int nbJetons = 0;
		for (int ligne = 0; ligne < height; ligne++) {
			if (grille[ligne][colonne] == symb)
				nbJetons++;
		}
		if (nbJetons == 4)
			return true;
	}
	return false;
}
//DIAGONALE
bool GrillePuissance4::DiagonaleComplete(const char& symb) const {
	return (
		(grille[3][0] == grille[2][1] && grille[3][0] == grille[1][2] && grille[3][0] == grille[0][3]) && grille[3][0] != ' ' ||
		(grille[3][1] == grille[2][2] && grille[3][1] == grille[1][3] && grille[3][1] == grille[0][4]) && grille[3][1] != ' ' ||
		(grille[3][2] == grille[2][3] && grille[3][2] == grille[1][4] && grille[3][2] == grille[0][5]) && grille[3][2] != ' ' ||
		(grille[3][3] == grille[2][4] && grille[3][3] == grille[1][5] && grille[3][3] == grille[0][6]) && grille[3][3] != ' ' ||

		(grille[3][6] == grille[2][5] && grille[3][6] == grille[1][4] && grille[3][6] == grille[0][6]) && grille[3][6] != ' ' ||
		(grille[3][5] == grille[2][4] && grille[3][5] == grille[1][3] && grille[3][5] == grille[0][5]) && grille[3][5] != ' ' ||
		(grille[3][4] == grille[2][3] && grille[3][4] == grille[1][2] && grille[3][4] == grille[0][4]) && grille[3][4] != ' ' ||
		(grille[3][3] == grille[2][2] && grille[3][3] == grille[1][1] && grille[3][3] == grille[0][3]) && grille[3][3] != ' '
		);
}
//GRILLE PLEINE
bool GrillePuissance4::MatchNul() const {
	for (int ligne = 0; ligne < width; ligne++) {
		for (int colonne = 0; colonne < height; colonne++) {
			if (grille[ligne][colonne] == ' ')
				return false;
		}
	}
	return true;
}

bool GrillePuissance4::VictoireJoueur(const char& symb) const {
	return(LigneComplete(symb) ||
		ColonneComplete(symb) ||
           DiagonaleComplete(symb));
}


void GrillePuissance4::AfficheGrille() const {
	cout << endl;
	cout << " 0   1   2   3   4   5   6\n";
	for (int a = 0; a < height; a++)
	{
		for (int b = 0; b < width; b++) cout << char(218) << char(196) << char(191) << " ";
		cout << '\n';
		for (int b = 0; b < width; b++) cout << char(179) << grille[a][b] << char(179) << " ";
		cout << '\n';
		for (int b = 0; b < width; b++) cout << char(192) << char(196) << char(217) << " ";
		cout << '\n';
	}
}
