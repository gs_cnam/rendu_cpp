#ifndef Player_hpp
#define Player_hpp

#include <iostream>
#include "iPlayer.h"

class Player: public iPlayer
{
public:
    Player(std::string inputName, char inputSymbol);
    ~Player(){};
	
    char GetSymbol() override;
	std::string GetNom() override;
    std::string GetPlayerData() override;
};
#endif /* Player_hpp */
