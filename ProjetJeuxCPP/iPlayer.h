#pragma once

#include <iostream>
class iPlayer
{
public:
	virtual char GetSymbol() = 0;
	virtual std::string GetNom() = 0;
	virtual std::string GetPlayerData() = 0;

protected:
	std::string name;
	char symbol;
};

